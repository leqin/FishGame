﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishAtt : MonoBehaviour
{
    public int hp;
    public int exp;
    public int gold;
    public int maxNum;//最大生成个数
    public int maxSpeed;//最大速度
    public GameObject dieAnimater;
    public GameObject glodPrefab;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Border")
        {
            Destroy(gameObject);
        }
    }

    void TakeDamage(int value)
    {
        //TODO
        //print("命中");
        hp -= value;
        if (hp <= 0)
        {
            GameController.Instance.exp += exp;
            GameController.Instance.gold += gold;
            GameObject diePrefab = Instantiate(dieAnimater);
            diePrefab.transform.SetParent(gameObject.transform.parent, false);
            diePrefab.transform.position = gameObject.transform.position;
            diePrefab.transform.rotation = gameObject.transform.rotation;
            GameObject glodPre = Instantiate(glodPrefab);
            glodPre.transform.SetParent(gameObject.transform.parent, false);
            glodPre.transform.position = gameObject.transform.position;
            glodPre.transform.rotation = gameObject.transform.rotation;
            if (gameObject.GetComponent<PlayEffect>()!=null)
            {
                gameObject.GetComponent<PlayEffect>().PlayEffects();
                AudioManager.Instance.PlayEffectSound(AudioManager.Instance.goldClip);
            }
            Destroy(gameObject);
        }
    }

}
