﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletAtt : MonoBehaviour
{
    public int speed=10;
    public int damage=5;
    public GameObject webPrefabs;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag=="Border")
        {
            Destroy(gameObject);
        }
        if (collision.tag=="Fish")
        {
            GameObject web = Instantiate(webPrefabs);
            AudioManager.Instance.PlayEffectSound(AudioManager.Instance.openWeb);
            web.transform.SetParent(gameObject.transform.parent,false);
            web.transform.position = gameObject.transform.position;
            web.GetComponent<WebAtt>().damage = damage;
            Destroy(gameObject);
        }
    }

}

