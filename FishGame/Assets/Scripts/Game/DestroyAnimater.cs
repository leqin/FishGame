﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAnimater : MonoBehaviour
{
    public float delayTime = 0.7f;
    void Start()
    {
        Destroy(gameObject, delayTime);
    }

}
