﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunFllow : MonoBehaviour
{
    public RectTransform UGUICanvas;//当前Canvas下的RectTransform
    public Camera mainCamera;//渲染主相机
    Vector3 worldPos;//屏幕鼠标位置转换的时间坐标位置
    float z;//z轴旋转
    void Start()
    {
        UGUICanvas = GameObject.Find("Order1Canvas").GetComponent<RectTransform>();
        mainCamera = Camera.main;
    }

    void Update()
    {
        if (Time.timeScale==0)
        {
            return;
        }
        //鼠标位置转为当前Canvas局部坐标下的世界坐标
        RectTransformUtility.ScreenPointToWorldPointInRectangle(UGUICanvas,new Vector2( Input.mousePosition.x,Input.mousePosition.y),mainCamera,out worldPos);
        //鼠标点在炮的左面，z轴旋转为正值,否则为负值
        if (transform.position.x> worldPos.x)
        {
           z= Vector3.Angle(Vector3.up,worldPos-transform.position);
        }
        else
        {
            z = -Vector3.Angle(Vector3.up, worldPos - transform.position);
        }
        if (z>=-80&&z<=80)
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, z));
        }
    }
}
