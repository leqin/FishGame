﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public int Lv = 0;//当前等级；
    public int exp = 0;
    public int gold = 500;
    public const int bigCountdown = 240;
    public const int smallCountdown = 60;
    public float bigTimer = bigCountdown;
    public float smallTimer = smallCountdown;

    [Header("开火特效")]
    public GameObject FireEffect;//开火特效
    [Header("换枪特效")]
    public GameObject ChangeGunEffect;//换枪特效
    [Header("升级特效")]
    public GameObject LvUpEffect;//升级特效
    [Header("得到金币特效")]
    public GameObject goldEffect;//金币奖励特效
    [Header("水浪特效")]
    public GameObject seaWaveEffect;//切换背景水浪特效


    public GameObject[] GunGos;
    public Text oneShootCostsText;
    public Transform bulletHolder;
    public GameObject[] bullet1Gos;
    public GameObject[] bullet2Gos;
    public GameObject[] bullet3Gos;
    public GameObject[] bullet4Gos;
    public GameObject[] bullet5Gos;
    public Color goldColor;//金币文字的原始颜色

    int costIndex = 0;//使用的是第几档炮弹
    int[] oneShootCosts = { 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000 };
    public string[] LvNames = { "新手", "入门", "钢铁", "青铜", "白银", "黄金", "铂金", "钻石", "大师", "宗师" };

    private int _bgIndex = 0;//当前背景精灵的索引

    static GameController instance;
    public static GameController Instance
    {
        get { return instance; }
    }

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        gold = PlayerPrefs.GetInt("gold", gold);
        Lv = PlayerPrefs.GetInt("lv", Lv);
        smallTimer = PlayerPrefs.GetFloat("scd", smallTimer);
        bigTimer = PlayerPrefs.GetFloat("bcd", bigTimer);
        exp = PlayerPrefs.GetInt("exp", exp);


        oneShootCostsText = GameObject.Find("oneShootCostsText").GetComponent<Text>();
        bulletHolder = GameObject.Find("BulletHolder").transform;
    }

    private void Update()
    {
        OnChangeGunByMouse();
        Fire();
        UpdateData();
        UpdateBg();
    }

    void UpdateBg()
    {
        if (_bgIndex != Lv / 20)
        {
            _bgIndex = Lv / 20;
            Instantiate(seaWaveEffect);
            AudioManager.Instance.PlayEffectSound(AudioManager.Instance.seaWaveClip);
            if (_bgIndex > 3)
            {
                UiManager.Instance.ChangeBg(_bgIndex);
            }
            else
            {
                UiManager.Instance.ChangeBg(_bgIndex);
            }
        }
    }

    void UpdateData()
    {
        bigTimer -= Time.deltaTime;
        smallTimer -= Time.deltaTime;
        if (smallTimer <= 0)
        {
            smallTimer = smallCountdown;
            gold += 50;
        }

        //经验等级换算公式:升级所需经验=1000+200*当前等级
        while (exp >= 1000 + 200 * Lv)
        {
            Lv++;
            Instantiate(LvUpEffect);
            AudioManager.Instance.PlayEffectSound(AudioManager.Instance.lvUpClip);
            StartCoroutine(UiManager.Instance.LvUpUI());
            exp = exp - (1000 + 200 * Lv);
        }

    }

    public void UpdateGoldAndBigTimer()
    {
        Instantiate(goldEffect);
        gold += 500;
        AudioManager.Instance.PlayEffectSound(AudioManager.Instance.rewardClip);
        bigTimer = bigCountdown;
    }

    void Fire()
    {
        if (Time.timeScale == 0)
            return;
        GameObject[] useBullets = bullet1Gos;
        int bulletIndex;
        if (Input.GetMouseButtonDown(0) && EventSystem.current.IsPointerOverGameObject() == false)
        {
            if (gold >= oneShootCosts[costIndex])
            {
                gold -= oneShootCosts[costIndex];
                Instantiate(FireEffect, GunGos[costIndex / 4].transform.Find("FirePos").transform.position,
                    Quaternion.Euler(GunGos[costIndex / 4].transform.Find("FirePos").transform.up));
                AudioManager.Instance.PlayEffectSound(AudioManager.Instance.fireClip);
                switch (costIndex / 4)
                {
                    case 0: useBullets = bullet1Gos; break;
                    case 1: useBullets = bullet2Gos; break;
                    case 2: useBullets = bullet3Gos; break;
                    case 3: useBullets = bullet4Gos; break;
                    case 4: useBullets = bullet5Gos; break;
                    default: useBullets = bullet1Gos; break;
                }
                bulletIndex = (Lv % 10 > 9) ? 9 : Lv % 10;//等级越高，子弹的索引越大，越华丽
                GameObject bullet = GameObject.Instantiate(useBullets[bulletIndex]);
                bullet.transform.SetParent(bulletHolder, false);
                bullet.transform.position = GunGos[costIndex / 4].transform.Find("FirePos").transform.position;
                bullet.transform.rotation = GunGos[costIndex / 4].transform.Find("FirePos").transform.rotation;
                bullet.GetComponent<BulletAtt>().damage = oneShootCosts[costIndex];
                bullet.AddComponent<AutoMove>().Dir = Vector3.up;
                bullet.GetComponent<AutoMove>().speed = bullet.GetComponent<BulletAtt>().speed;
            }
            else
            {
                //提示金币不足
                StartCoroutine(GoldNotEnough());
                AudioManager.Instance.PlayEffectSound(AudioManager.Instance.goldNotEnough);
            }
        }
    }

    IEnumerator GoldNotEnough()
    {
        UiManager.Instance.goldText.color = Color.red;
        yield return new WaitForSeconds(0.5f);
        UiManager.Instance.goldText.color = goldColor;
    }

    void OnChangeGunByMouse()
    {
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            OnButtonReduceDown();
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            OnButtonAddDown();
        }
    }

    public void OnButtonAddDown()
    {
        GunGos[costIndex / 4].SetActive(false);
        costIndex++;
        AudioManager.Instance.PlayEffectSound(AudioManager.Instance.changeGunClip);
        Instantiate(ChangeGunEffect);
        costIndex = costIndex > oneShootCosts.Length - 1 ? oneShootCosts.Length - 1 : costIndex;
        GunGos[costIndex / 4].SetActive(true);
        oneShootCostsText.text = "$" + oneShootCosts[costIndex];
    }

    public void OnButtonReduceDown()
    {
        GunGos[costIndex / 4].SetActive(false);
        costIndex--;
        AudioManager.Instance.PlayEffectSound(AudioManager.Instance.changeGunClip);
        Instantiate(ChangeGunEffect);
        costIndex = costIndex < 0 ? 0 : costIndex;
        GunGos[costIndex / 4].SetActive(true);
        oneShootCostsText.text = "$" + oneShootCosts[costIndex];
    }
}
