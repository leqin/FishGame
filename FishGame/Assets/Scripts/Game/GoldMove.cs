﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldMove : MonoBehaviour
{
    Transform _goldCollect;

    private void Start()
    {
        _goldCollect = GameObject.Find("GoldCollect").transform;
    }

    private void Update()
    {
       transform.position= Vector3.MoveTowards(gameObject.transform.position, _goldCollect.position, 10 * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag=="GoldCollect")
        {
            Destroy(gameObject);
        }
    }
}
