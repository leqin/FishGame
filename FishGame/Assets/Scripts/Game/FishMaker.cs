﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class FishMaker : MonoBehaviour
{

    public GameObject[] fishPrefabs;
    public Transform[] fishBornPos;
    public Transform fishHolder;
    public float fishGenWaitTime = 0.5f;//生成鱼的时间间隔
    public float waveGenWaitTime = 0.3f;//每0.3秒生成一波鱼

    //int genPosIndex;
    //int fishPrefabIndex;
    //int maxNum;
    //int maxSpeed;
    //int num;
    //int speed;
    //int moveType;

    void Start()
    {
        if (Directory.Exists("Assets/Prefabs/Fish"))
        {
            DirectoryInfo direction = new DirectoryInfo("Assets/Prefabs/Fish");
            FileInfo[] files = direction.GetFiles("*", SearchOption.AllDirectories);
            fishPrefabs = new GameObject[files.Length / 2];
            int j = 0;
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].Name.EndsWith(".prefab"))
                {
#if UNITY_EDITOR
                    fishPrefabs[j] = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Fish/" + files[i].Name);
#endif
                    j++;
                }
            }
        }

        fishBornPos = GameObject.Find("GeneratePos").transform.GetComponentsInChildren<Transform>();
        fishHolder = GameObject.Find("FishHolder").transform;

        InvokeRepeating("MakeFishes", 0, waveGenWaitTime);
    }

    void MakeFishes()
    {
        int genPosIndex = Random.Range(1, fishBornPos.Length);
        int fishPrefabIndex = Random.Range(0, fishPrefabs.Length);
        int maxNum = fishPrefabs[fishPrefabIndex].GetComponent<FishAtt>().maxNum;
        int maxSpeed = fishPrefabs[fishPrefabIndex].GetComponent<FishAtt>().maxSpeed;
        int num = Random.Range((maxNum / 2) + 1, maxNum + 1);
        int speed = Random.Range(maxSpeed / 2, maxSpeed + 1);
        int moveType = Random.Range(0, 2);//0直走，1转弯
        int angOffset;//直走的倾斜角
        int angSpeed;//转弯的角速度

        if (moveType == 0)
        {
            //直走
            angOffset = Random.Range(-22, 23);
            StartCoroutine(BornStraightFish(genPosIndex, fishPrefabIndex, num, speed, angOffset));
        }
        else
        {
            //转弯
            if (Random.Range(0, 2) == 0)
            {
                angSpeed = Random.Range(-15, -9);
            }
            else
            {
                angSpeed = Random.Range(9, 15);
            }
            StartCoroutine(BornWheelFish(genPosIndex, fishPrefabIndex, num, speed, angSpeed));
        }
    }

    IEnumerator BornStraightFish(int genPosIndex, int fishPrefabsIndex, int num, int speed, int angOffset)
    {
        for (int i = 0; i < num; i++)
        {
            var fish = Instantiate(fishPrefabs[fishPrefabsIndex]);
            fish.transform.SetParent(fishHolder, false);//第二个参数设置为false时，unity不会重新计算坐标
            fish.transform.localPosition = fishBornPos[genPosIndex].localPosition;
            fish.transform.localRotation = fishBornPos[genPosIndex].localRotation;
            fish.transform.Rotate(0, 0, angOffset);
            fish.GetComponent<SpriteRenderer>().sortingOrder += i;//每生成一条鱼，渲染层级加1
            fish.AddComponent<AutoMove>().speed = speed;
            yield return new WaitForSeconds(fishGenWaitTime);
        }
    }

    IEnumerator BornWheelFish(int genPosIndex, int fishPrefabsIndex, int num, int speed, int angSpeed)
    {
        for (int i = 0; i < num; i++)
        {
            var fish = Instantiate(fishPrefabs[fishPrefabsIndex]);
            fish.transform.SetParent(fishHolder, false);//第二个参数设置为false时，unity不会重新计算坐标
            fish.transform.localPosition = fishBornPos[genPosIndex].localPosition;
            fish.transform.localRotation = fishBornPos[genPosIndex].localRotation;
            fish.GetComponent<SpriteRenderer>().sortingOrder += i;//每生成一条鱼，渲染层级加1
            var autoMove = fish.AddComponent<AutoMove>();
            autoMove.speed = speed;
            autoMove.rotate = true;
            autoMove.rotateSpeed = angSpeed;
            yield return new WaitForSeconds(fishGenWaitTime);
        }
    }
}
