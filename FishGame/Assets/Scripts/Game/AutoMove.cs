﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMove : MonoBehaviour
{

    public float speed=10;             //移动速度
    public float rotateSpeed = 10;     //旋转速度
    public Vector3 Dir = Vector3.right;//移动方向
    public bool rotate = false;        //旋转

    public bool isAutoDestroy;         //是否自动销毁
    public float delayDestroyTime=3;      //销毁延时
    void Update()
    {
        transform.Translate(Dir * speed * Time.deltaTime);
        if (rotate)
        {
            transform.Rotate(Vector3.forward, rotateSpeed * Time.deltaTime);
        }
        if (isAutoDestroy)
        {
            Destroy(gameObject, delayDestroyTime);
        }
    }
}
