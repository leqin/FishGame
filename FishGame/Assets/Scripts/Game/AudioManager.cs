﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    static AudioManager _instance;
    public static AudioManager Instance
    {
        get
        {
            return _instance;
        }
    }

    [Header("背景音频")]
    public AudioSource bgmAudioSotrce;  //背景音频
    [Header("海浪声音")]
    public AudioClip seaWaveClip;       //海浪声音
    [Header("金币声音")]
    public AudioClip goldClip;          //金币声音
    [Header("奖励声音")]
    public AudioClip rewardClip;        //奖励声音
    [Header("开火声音")]
    public AudioClip fireClip;          //开火声音
    [Header("换枪声音")]
    public AudioClip changeGunClip;     //换枪声音
    [Header("升级声音")]
    public AudioClip lvUpClip;          //升级声音
    [Header("金币不足")]
    public AudioClip goldNotEnough;     //金币不足提示音
    [Header("开网")]
    public AudioClip openWeb;


    private void Awake()
    {
        _instance = this;
        bgmAudioSotrce.mute = (PlayerPrefs.GetInt("mute", 0) == 0) ? false : true;
        bgmAudioSotrce.volume = PlayerPrefs.GetFloat("volume", 1);
    }

    /// <summary>
    /// 播放声音，大小和静音和背景音乐相同
    /// </summary>
    /// <param name="clip"></param>
    public void PlayEffectSound(AudioClip clip)
    {
        if (bgmAudioSotrce.mute)
            return;
        AudioSource.PlayClipAtPoint(clip, new Vector3(0, 0, -5), bgmAudioSotrce.volume);
    }

}
