﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainSceneUI : MonoBehaviour
{
    private Button _settingButton;
    private Button _backButton;
    public GameObject setPanel;

    private void Start()
    {
        _settingButton = GameObject.Find("SetButton").GetComponent<Button>();
        _backButton = GameObject.Find("BackButton").GetComponent<Button>();
        UiManager.Instance.AddListener(_settingButton,Setting);
        UiManager.Instance.AddListener(_backButton,Back);
    }

    void Setting()
    {
        //Debug.Log("设置");
        Debug.Log(Time.timeScale);
        Time.timeScale = 0;
        setPanel.SetActive(true);
    }

    void Back()
    {
        //Debug.Log("返回开始界面");
        PlayerPrefs.SetInt("gold",GameController.Instance.gold);
        PlayerPrefs.SetInt("lv", GameController.Instance.Lv);
        PlayerPrefs.SetFloat("scd", GameController.Instance.smallTimer);
        PlayerPrefs.SetFloat("bcd", GameController.Instance.bigTimer);
        PlayerPrefs.SetInt("exp", GameController.Instance.exp);
        PlayerPrefs.SetInt("mute", (AudioManager.Instance.bgmAudioSotrce.mute == false) ? 0 : 1);
        PlayerPrefs.SetFloat("volume", AudioManager.Instance.bgmAudioSotrce.volume);
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

}
