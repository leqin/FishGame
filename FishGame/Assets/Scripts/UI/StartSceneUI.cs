﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartSceneUI : MonoBehaviour
{

    private Button _beginButton;
    private Button _continueButton;
    private Button _quitButton;

    private void Start()
    {
        _beginButton = GameObject.Find("begingame").GetComponent<Button>();
        _continueButton = GameObject.Find("continuegame").GetComponent<Button>();
        _quitButton = GameObject.Find("QuitButton").GetComponent<Button>();
        _beginButton.onClick.AddListener(NewGame);
        _continueButton.onClick.AddListener(ContinueGame);
        _quitButton.onClick.AddListener(Quit);
    }

    void NewGame()
    {
        //除了声音之外的键值全部删除
        PlayerPrefs.DeleteKey("gold");
        PlayerPrefs.DeleteKey("lv");
        PlayerPrefs.DeleteKey("scd");
        PlayerPrefs.DeleteKey("bcd");
        PlayerPrefs.DeleteKey("exp");

        //PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(1);
    }

    void ContinueGame()
    {
        SceneManager.LoadScene(1);
    }

    private void Quit()
    {
        Application.Quit();
    }
}
