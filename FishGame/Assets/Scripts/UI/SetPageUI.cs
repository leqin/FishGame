﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetPageUI : MonoBehaviour {

    private Button _setBackButton;
    private Slider _audioSlider;
    private Toggle _audioToggle;

	void Start () {
        _setBackButton = gameObject.transform.Find("SetBackButton").GetComponent<Button>();
        _audioSlider = gameObject.transform.Find("AudioSlider").GetComponent<Slider>();
        _audioToggle = gameObject.transform.Find("SwitchToggle").GetComponent<Toggle>();
        _audioSlider.value=AudioManager.Instance.bgmAudioSotrce.volume;
        _audioToggle.isOn = !AudioManager.Instance.bgmAudioSotrce.mute;

        UiManager.Instance.AddListener(_setBackButton,SetBack);
        _audioToggle.onValueChanged.AddListener(ChangeToggle);
    }

	void Update () {
        AudioManager.Instance.bgmAudioSotrce.volume = _audioSlider.value;
	}

    void SetBack()
    {
        gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    void ChangeToggle(bool b)
    {
        // Debug.Log(b);
        AudioManager.Instance.bgmAudioSotrce.mute=!b;
    }

}
