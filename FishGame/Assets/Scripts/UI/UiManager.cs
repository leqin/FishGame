﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    public Text oneShootCostText;
    public Text goldText;
    public Text lvText;
    public Text lvNameText;
    public Text smallCountdownText;   //小倒计时
    public Text bigCountdownText;     //大倒计时
    public Button bigCountdownButton; //倒计时到达时间时的奖励按钮
    public Button backButton;
    public Button settingButton;
    public Slider expSlider;
    public Image Lvup;                //升级提示框
    static UiManager instance;

    public Image bg;                  //背景图
    public Sprite[] bgSprites;        //背景图精灵


    public static UiManager Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        instance = this;
        AddListener(bigCountdownButton, GetAwards);
    }

    public void AddListener(Button button,UnityEngine.Events.UnityAction call)
    {
        button.onClick.AddListener(call);
    }

    public IEnumerator LvUpUI()
    {
        Lvup.gameObject.SetActive(true);
        Lvup.gameObject.transform.Find("Text").GetComponent<Text>().text = GameController.Instance.Lv.ToString();
        yield return new WaitForSeconds(0.5f);
        Lvup.gameObject.SetActive(false);
    }

    void GetAwards()
    {
        // print("奖励按钮按下");
        GameController.Instance.UpdateGoldAndBigTimer();
        bigCountdownButton.gameObject.SetActive(false);
        bigCountdownText.gameObject.SetActive(true);
    }

    void Update()
    {
        UpdateUI();
    }

    void UpdateUI()
    {
        goldText.text = "$" + GameController.Instance.gold;
        lvText.text = GameController.Instance.Lv.ToString();
        if (GameController.Instance.Lv / 10 <= 9)
        {
            lvNameText.text = GameController.Instance.LvNames[GameController.Instance.Lv / 10];
        }
        else
        {
            lvNameText.text = GameController.Instance.LvNames[9];
        }
        smallCountdownText.text = " " + (int)GameController.Instance.smallTimer / 10 + " " + (int)GameController.Instance.smallTimer % 10;
        bigCountdownText.text = (int)GameController.Instance.bigTimer + "s";
        expSlider.value = ((float)GameController.Instance.exp) / (1000 + 200 * GameController.Instance.Lv);
        if (GameController.Instance.bigTimer <= 0 && bigCountdownButton.gameObject.activeSelf == false)
        {
            bigCountdownText.gameObject.SetActive(false);
            bigCountdownButton.gameObject.SetActive(true);
        }
    }

    public void ChangeBg(int index)
    {
        bg.sprite = bgSprites[index];
    }

}
