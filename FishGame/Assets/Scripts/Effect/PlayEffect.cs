﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayEffect : MonoBehaviour
{
    public GameObject[] effectPrefabs;
    public void PlayEffects()
    {
        foreach (GameObject item in effectPrefabs)
        {
            Instantiate(item);
        }
    }
}
