﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectWave : MonoBehaviour {

    public Texture[] textures;
    private Material _material;
    private int index;

	void Start () {
        _material = gameObject.GetComponent<MeshRenderer>().material;
        InvokeRepeating("ChangeTexture",0,0.1f);
	}
	
	void ChangeTexture()
    {
        _material.mainTexture = textures[index];
        index = (index + 1) % textures.Length;
    }
}
